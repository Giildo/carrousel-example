import './style.css'

const images = document.querySelectorAll('img')
const imageNb = images.length
let currentImage = 0

const buttons = document.querySelectorAll('header button')
const update = () => {
  images.forEach((image, index) => {
    image.style.left = `${100 * (index - currentImage)}%`
  })
}
update()

buttons[0]!.addEventListener('click', () => {
  currentImage = (currentImage - 1 + imageNb) % imageNb
  update()
})

buttons[1]!.addEventListener('click', () => {
  currentImage = (currentImage + 1) % imageNb
  update()
})